package com.ptc.ccp.vertxservices.reactive;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Supplier;

import com.google.gson.Gson;
import com.ptc.ccp.vertx.utils.VertxContextScheduler;
import com.ptc.ccp.vertx.utils.VertxCompletableFuture;
import com.ptc.microservices.reactive.GsonReactiveConnector;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;

import io.vertx.core.Vertx;
import io.reactivex.Scheduler;

public abstract class VertxReactiveConnector extends GsonReactiveConnector
{
	@SettingProperty (defaultValue="false", cmdLineName="connectorlogging", documentation="Turns on/off connector logging (default is Off).")
	protected static boolean ConnectorLoggingOn;
	
	protected Vertx vertx;
	
	protected VertxReactiveConnector (Vertx vertx, Gson gson, Runnable disconnector)
	{
		super (gson, disconnector);
		this.vertx = vertx;
		ApplicationProperties.processStaticProperties (VertxReactiveConnector.class);
	}
	
	public Vertx vertx ()
	{
		return vertx;
	}

	@Override
	public <T> CompletableFuture<T> newCompletableFuture ()
	{
		return new VertxCompletableFuture<> (vertx);
	}
	
	@Override
	public <T> CompletableFuture<T> newCompletableFuture (Supplier<T> supplier)
	{
		return VertxCompletableFuture.supplyAsync (supplier);
	}
	
	@Override
	public <T> CompletableFuture<T> newCompletableFuture (Supplier<T> supplier, Executor executor)
	{
		return VertxCompletableFuture.supplyAsync (supplier, executor);
	}

	@Override
	public Scheduler defaultScheduler ()
	{
		return VertxContextScheduler.scheduler (vertx);
	}
	
	protected void logInfo (String msg)
	{
		if (ConnectorLoggingOn) {
			System.out.println (msg);
		}
	}

	protected void logError (String msg, Throwable err)
	{
		System.out.println (msg + ": " + (err != null ? err.getMessage () : ""));
	}

}
