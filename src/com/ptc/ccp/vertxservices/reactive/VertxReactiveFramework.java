package com.ptc.ccp.vertxservices.reactive;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICountDownLatch;
import com.hazelcast.core.LifecycleEvent;
import com.ptc.ccp.vertx.utils.VertxContextScheduler;
import com.ptc.ccp.vertxservices.AbstractPTCVerticle;
import com.ptc.microservices.reactive.IReactiveFramework;
import com.ptc.microservices.reactive.IReactiveServer;
import com.ptc.microservices.reactive.ReactiveClient;

import io.reactivex.Scheduler;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class VertxReactiveFramework implements IReactiveFramework
{
	private static void beforeLeaveUndeploy (Vertx vertx, ICountDownLatch latch)
	{
		AbstractPTCVerticle.logStaticInfo ("*> Undeploy all verticles before terminating:");
		for (String depId : vertx.deploymentIDs ()) {
			AbstractPTCVerticle.logStaticInfo ("*>  Terminating verticle " + depId);
			try {
				vertx.undeploy (depId);
			}
			catch (Throwable ex) {
				System.err.println (ex);
			}
		}
	}

	private static Vertx nonclusteredVertx;

	static Vertx getNonclusteredVertx ()
	{
		return getNonclusteredVertx (null);
	}

	static synchronized Vertx getNonclusteredVertx (VertxOptions vops)
	{
		if (nonclusteredVertx == null) {
			if (vops == null) {
				vops = new VertxOptions ();
			}
			nonclusteredVertx = Vertx.vertx (vops);
		}
		return nonclusteredVertx;
	}

	private static CompletableFuture<Vertx> clusteredVertx;

	static Vertx getClusteredVertx ()
	{
		return getClusteredVertx (AbstractPTCVerticle.buildVertxOptions ());
	}

	public static Scheduler vertxScheduler ()
	{
		return VertxContextScheduler.scheduler ((clusteredVertx != null) ? getClusteredVertx ()
				: (nonclusteredVertx != null) ? nonclusteredVertx : getClusteredVertx ());
	}

	private static String toFormattedString (VertxOptions vops)
	{
		/*
		 * JsonObject jops = new JsonObject (vops.toString ()); return
		 * jops.encodePrettily ();
		 */
		return vops.toString ();
	}

	static synchronized Vertx getClusteredVertx (VertxOptions vops)
	{
		if (clusteredVertx == null) {
			if (vops == null) {
				vops = new VertxOptions ();
			}
			clusteredVertx = new CompletableFuture<Vertx> ();
			final VertxOptions vertxOptions = vops;
			Vertx.clusteredVertx (vops, res -> {
				if (res.succeeded ()) {
					Vertx newvertx = res.result ();
					ClusterManager clusterManager = vertxOptions.getClusterManager ();
					AbstractPTCVerticle
							.logStaticInfo ("getClusteredVertx with options \n" + toFormattedString (vertxOptions));
					if (AbstractPTCVerticle.getUndeployOnShutdown ()
							&& clusterManager instanceof HazelcastClusterManager) {
						AbstractPTCVerticle.logStaticInfo ("Configuring beforeLeaveUndeploy");
						HazelcastInstance hzcInstance = ((HazelcastClusterManager) clusterManager)
								.getHazelcastInstance ();
						hzcInstance.getLifecycleService ().addLifecycleListener (state -> {
							if (state.getState () == LifecycleEvent.LifecycleState.SHUTTING_DOWN) {
								ICountDownLatch latch = hzcInstance.getCountDownLatch ("shutdown.latch");
								latch.trySetCount (1);
								beforeLeaveUndeploy (newvertx, latch);
							}
						});
					}
					clusteredVertx.complete (newvertx);
				}
				else {
					System.err.println ("Failed to initialize clustered Vert.x instance");
					res.cause ().printStackTrace ();
				}
			});
		}
		try {
			return clusteredVertx.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			System.err.println ("Failed to obtain clustered Vert.x instance");
			e.printStackTrace (System.err);
			return null;
		}
	}

	public static Vertx getVertxInstance (VertxOptions vops)
	{
		// AbstractPTCVerticle.logStaticInfo ("getVertxInstance with options \n"
		// + vops);
		return ((vops != null) && vops.isClustered ()) ? getClusteredVertx (vops) : getNonclusteredVertx (vops);
	}

	private static void closeVertx (Vertx vertx)
	{
		vertx.close ();
	}

	@Override
	public void shutdown ()
	{
		scheduler ().scheduleDirect (() -> doShutdown ());
	}

	private synchronized void doShutdown ()
	{
		if (nonclusteredVertx != null) {
			closeVertx (nonclusteredVertx);
			nonclusteredVertx = null;
		}
		if (clusteredVertx != null) {
			try {
				closeVertx (clusteredVertx.get ());
			}
			catch (Throwable e) {
				System.out.println ("XPEH");
				// e.printStackTrace (); -- IGNORE
			}
			clusteredVertx = null;
		}
	}

	@Override
	public <API extends ReactiveClient<API>> void connectToServer (String serviceName, Class<API> apiClass,
			Consumer<API> connectionHandler, Consumer<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException
	{
		EbusReactiveConnector.connectToServer (serviceName, apiClass, connectionHandler,
				failure -> failHandler.accept (failure));
	}

	@Override
	public IReactiveServer createServer (String[] args, Gson gson)
	{
		return VertxReactiveServer.createInstance (args, gson);
	}

	@Override
	public Scheduler scheduler ()
	{
		return vertxScheduler ();
	}
}
