package com.ptc.ccp.vertxservices.reactive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.ptc.ccp.vertxservices.TypedAbstractRestService.AccessMethod;
import com.ptc.microservices.reactive.IReactiveServer;
import com.ptc.microservices.reactive.ReactiveService;
import com.ptc.microservices.reactive.ReactiveStringRequestProcessor;
import com.ptc.microservices.reactive.RequestResponseSerializer;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.CommandLineArguments;

import io.reactivex.Scheduler;

public class VertxReactiveServer implements IReactiveServer
{
	@SettingProperty (defaultValue = "0", cmdLineName = "reactivewebport", envVariableName = "REACTIVE_WEB_PORT", documentation = "For reactive web services running as web service.")
	protected static int HttpPort;

	private static CommandLineArguments arguments;

	private List<ReactiveServer> servers;
	private HashMap<String, ReactiveServer> serversMap;
	private Gson gson;

	protected VertxReactiveServer (Gson gson)
	{
		super ();
		this.servers = new ArrayList<> ();
		this.serversMap = new HashMap<> ();
		this.gson = (gson != null ? gson : RequestResponseSerializer.GetDefaultGson ());
	}

	protected VertxReactiveServer ()
	{
		this (RequestResponseSerializer.GetDefaultGson ());
	}

	public static VertxReactiveServer createInstance (String[] args, Gson gson)
	{
		if (arguments == null) {
			String propFile = null;
			arguments = new CommandLineArguments (args);
			if (arguments.hasArgument ("config")) {
				propFile = arguments.getArgument ("config");
			}
			System.out.println ("Using properties file: " + propFile);
			ApplicationProperties.loadProperties (propFile, arguments, VertxReactiveServer.class);
		}

		return new VertxReactiveServer (gson);
	}

	public static VertxReactiveServer createInstance (String[] args)
	{
		return createInstance (args, null);
	}

	// -------------------------------------------------------------------------------------------------------
	private HashMap<ReactiveService, ReactiveServer> serviceToServerMap = new HashMap<> ();

	/* (non-Javadoc)
	 * @see com.ptc.ccp.vertxservices.reactive.IReactiveServer#registerEventBusService(java.lang.String, com.ptc.microservices.reactive.ReactiveService)
	 */
	@Override
	public IReactiveServer registerEventBusService (String serviceName, ReactiveService serviceInstance)
	{
		ReactiveServer server = serviceToServerMap.get (serviceInstance);
		if (server == null) {
			ApplicationProperties.processStaticProperties (serviceInstance.getClass ());
			ReactiveStringRequestProcessor proc = new ReactiveStringRequestProcessor (gson, serviceInstance);
			server = new ReactiveServer (proc, AccessMethod.EventBus);
			serviceToServerMap.put (serviceInstance, server);
			final ReactiveServer finalServer = server;
			serviceInstance.setSchedulerSupplier ( () -> finalServer.scheduler ());
		}
		server.addAccessMethod (AccessMethod.EventBus);
		server.setBusServiceName (serviceName);
		serversMap.put (serviceName, server);
		servers.add (server);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.ptc.ccp.vertxservices.reactive.IReactiveServer#registerWebService(java.lang.String, int, java.lang.String, java.lang.String, com.ptc.microservices.reactive.ReactiveService)
	 */
	@Override
	public IReactiveServer registerWebService (String serviceName, int port, String path, String debugPath,
			ReactiveService serviceInstance)
	{
		ReactiveServer server = serviceToServerMap.get (serviceInstance);
		if (server == null) {
			ApplicationProperties.processStaticProperties (serviceInstance.getClass ());
			ReactiveStringRequestProcessor proc = new ReactiveStringRequestProcessor (
					RequestResponseSerializer.GetDefaultGson (), serviceInstance);
			server = new ReactiveServer (proc, port);
		}
		server.setWebPort (port);
		server.setWebServiceName (serviceName).setServerPath (path).setDebudPath (debugPath);
		serversMap.put (serviceName, server);
		servers.add (server);
		return this;
	}

	/* (non-Javadoc)
	 * @see com.ptc.ccp.vertxservices.reactive.IReactiveServer#registerWebService(java.lang.String, java.lang.String, com.ptc.microservices.reactive.ReactiveService)
	 * This is temporary override until HttpPort initialization in IReactiveServer
	 */
	@Override
	public IReactiveServer registerWebService (String serviceName, String path, ReactiveService serviceInstance)
	{
		return registerWebService (serviceName, HttpPort, path, null, serviceInstance);
	}

	// -------------------------------------------------------------------------------------------------------
	public ReactiveServer getService (String serviceName)
	{
		return serversMap.get (serviceName);
	}

	private final static CompletableFuture<?>[] EMPTY_ARRAY_OF_CFS = new CompletableFuture [0];

	// -------------------------------------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see com.ptc.ccp.vertxservices.reactive.IReactiveServer#start()
	 */
	@Override
	public CompletableFuture<Void> start ()
	{
		return CompletableFuture.allOf (servers.stream ().map (server -> server.startServer ())
				.collect (Collectors.toList ()).toArray (EMPTY_ARRAY_OF_CFS));
	}

	@Override
	public Scheduler scheduler ()
	{
		return VertxReactiveFramework.vertxScheduler ();
	}

}
