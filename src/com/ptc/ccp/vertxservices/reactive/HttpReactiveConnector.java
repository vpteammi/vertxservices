package com.ptc.ccp.vertxservices.reactive;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.function.Consumer;
import java.util.function.Function;

import com.google.gson.Gson;
import com.ptc.ccp.vertxservices.AbstractPTCVerticle;
import com.ptc.microservices.reactive.BasicServiceResponse;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.RequestResponseSerializer;
import com.ptc.microservices.reactive.ServiceMessage;
import com.ptc.microservices.reactive.ServiceResponse;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;

public class HttpReactiveConnector extends VertxReactiveConnector
{
	
	@SettingProperty (defaultValue="2000", documentation="Wait time for reply on HTTP request")
	private static long HttpRequestTimeoutMsec;
	
	static {
		ApplicationProperties.processStaticProperties (HttpReactiveConnector.class);
	}

	private final HttpClient httpClient;
	private final String     host;
	private final int        port;
	private final String     uri;

	public HttpReactiveConnector (Vertx vertx, String host, int port, String uri, Runnable disconnector, Gson gson)
	{
		super (vertx, gson, disconnector);
		this.httpClient = vertx.createHttpClient ();
		this.host = host;
		this.port = port;
		this.uri = uri;
		this.timeout = HttpRequestTimeoutMsec;
	}
	
	public HttpReactiveConnector (Vertx vertx, String host, int port, String uri, Runnable disconnector)
	{
		this (vertx, host, port, uri, disconnector, RequestResponseSerializer.GetDefaultGson ());
	}
	
	// -------------------------------------------------------------------
	public static <API extends ReactiveClient<API>> void connectToServer (String serviceName, Class<API> apiClass,
			Consumer<API> connectionHandler, Handler<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException
	{
		Constructor<API> apiConstructor = apiClass.getConstructor (IReactiveConnector.class);
		new AbstractPTCVerticle ()
		{
			@Override
			public void start () throws Exception
			{
				super.start ();
				findService (serviceName, this::onConnection, failHandler);
			}

			private void onConnection (JsonObject location)
			{
				String host = location.getString ("host", "localhost");
				int port = location.getInteger ("port");
				String uri = location.getString ("root");
				API apiClient;
				try {
					apiClient = apiConstructor.newInstance (new HttpReactiveConnector (vertx, host, port, uri, this::terminate));
					connectionHandler.accept (apiClient);
				}
				catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					failHandler.handle (e);
				}
			}

			public void run ()
			{
				runClustered (this, buildVertxOptions ());
			}
		}.run ();
	}
	
	public static <API extends ReactiveClient<API>> void connectToServer (URL service, Class<API> apiClass,
			Consumer<API> connectionHandler, Handler<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException
	{
		Constructor<API> apiConstructor = apiClass.getConstructor (IReactiveConnector.class);
		new AbstractPTCVerticle ()
		{
			@Override
			public void start () throws Exception
			{
				super.start ();
				connectToServer (service);
			}

			private void connectToServer (URL service)
			{
				String host = service.getHost ();
				int port = service.getPort ();
				String uri = service.getPath ();
				API apiClient;
				try {
					apiClient = apiConstructor.newInstance (new HttpReactiveConnector (vertx, host, port, uri, this::terminate));
					connectionHandler.accept (apiClient);
				}
				catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					failHandler.handle (e);
				}
			}

			public void run ()
			{
				runClustered (this, buildVertxOptions ());
			}
		}.run ();
	}
	
	// -------------------------------------------------------------------
	private <RESPONSE extends JsonSerializable> void processResponse (
			HttpClientResponse res, Consumer<ServiceResponse<RESPONSE>> handler, 
			Function<String, ServiceResponse<RESPONSE>> reader)
	{
		res.bodyHandler (buffer -> {
			logInfo ("Response (" + buffer.length () + "): ");
			String resp = buffer.getString (0, buffer.length ());
			logInfo (resp);
			if (handler != null) {
				try {
					ServiceResponse<RESPONSE> response = reader.apply (resp);
					response.setConnector (this); 
					handler.accept (response);
				}
				catch (Exception ex) {
				}
			}
		});
	}
	
	private <RESPONSE extends JsonSerializable> void processException (Throwable err, 
			Consumer<ServiceResponse<RESPONSE>> handler)
	{
		logError ("POST request failed", err);
		if (handler != null) {
			try {
				ServiceResponse<RESPONSE> response = new BasicServiceResponse<RESPONSE> (err);
				response.setConnector (this); 
				handler.accept (response);
			}
			catch (Exception ex) {
			}
		}
	}
	
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request, 
			Consumer<ServiceResponse<RESPONSE>> handler, Function<String, ServiceResponse<RESPONSE>> reader)
	{
		Buffer reqBuffer = Buffer.buffer (request.getBytes ());
		httpClient
			.post (port, host, uri, res -> processResponse (res, handler, reader))
			.setTimeout (getTimeout ())
			.exceptionHandler (err -> processException (err, handler))
			.putHeader (HttpHeaders.CONTENT_LENGTH, reqBuffer.length () + "")
			.putHeader (HttpHeaders.CONTENT_TYPE, "text/plain")
			.write (reqBuffer)
			.end ();
	}

	@SuppressWarnings ("unchecked")
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request, 
			Consumer<ServiceResponse<RESPONSE>> handler, Class<RESPONSE> responseCls)
	{
		sendRequest (request, handler, 
				body -> toResponse ((ServiceMessage<RESPONSE>) gson.fromJson (body, responseCls)));
	}

	@SuppressWarnings ("unchecked")
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request, 
			Consumer<ServiceResponse<RESPONSE>> handler, Type type)
	{
		sendRequest (request, handler, 
				body -> toResponse ((ServiceMessage<RESPONSE>) gson.fromJson (body, type)));
	}

}
