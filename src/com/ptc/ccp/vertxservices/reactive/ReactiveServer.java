package com.ptc.ccp.vertxservices.reactive;

import java.util.concurrent.CompletableFuture;

import com.ptc.ccp.vertxservices.StringRestService;
import com.ptc.microservices.IRequestProcessor;
import com.ptc.microservices.ServiceException;
import com.sm.javax.utilx.ApplicationProperties;

import io.vertx.core.Handler;

public class ReactiveServer extends StringRestService
{

	protected IRequestProcessor<String, String> requestProcessor;
	protected Runnable initializer;
	protected CompletableFuture<Void> startFuture;

	public ReactiveServer (IRequestProcessor<String, String> requestProcessor, AccessMethod method, int port)
	{
		super (method, port);
		this.requestProcessor = requestProcessor;
		this.startFuture = null;
	}

	public ReactiveServer (IRequestProcessor<String, String> requestProcessor, int port)
	{
		this (requestProcessor, AccessMethod.Web, port);
	}

	public ReactiveServer (IRequestProcessor<String, String> requestProcessor, AccessMethod method)
	{
		this (requestProcessor, method, DefaultHttpPort);
	}

	// ------------------------------------------------------------------------------
	public void setRequestProcessor (IRequestProcessor<String, String> requestProcessor)
	{
		this.requestProcessor = requestProcessor;
	}

	public void setInitializer (Runnable initializer)
	{
		this.initializer = initializer;
	}

	// ------------------------------------------------------------------------------
	@Override
	protected void initOnStart ()
	{
		if (initializer != null) {
			initializer.run ();
		}
	}

	@Override
	protected void executeRequest (String cmd, Handler<String> handler)
	{
		requestProcessor.executeRequest (cmd, response -> handler.handle (response));
	}

	// ------------------------------------------------------------------------------
	public CompletableFuture<Void> startServer (Runnable initializer)
	{
		if (startFuture == null) {
			startFuture = new CompletableFuture<> ();
			ApplicationProperties.processStaticProperties (ReactiveServer.class);
			setInitializer (initializer);
			runClustered (this, buildVertxOptions (), result -> {
				if (result.succeeded ())
				{
					startFuture.complete (null);
				}
				else
				{
					startFuture.completeExceptionally (new ServiceException ("Failed starting server"));
				}
			});
		}
		return startFuture;
	}

	public CompletableFuture<Void> startServer ()
	{
		return startServer (null);
	}
}
