package com.ptc.ccp.vertxservices.reactive;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;

import com.google.gson.Gson;
import com.ptc.ccp.vertxservices.AbstractPTCVerticle;
import com.ptc.microservices.reactive.BasicServiceResponse;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.RequestResponseSerializer;
import com.ptc.microservices.reactive.ServiceMessage;
import com.ptc.microservices.reactive.ServiceResponse;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.reactivex.Single;

public class EbusReactiveConnector extends VertxReactiveConnector
{
	private final EventBus ebus;
	private final String address;

	public EbusReactiveConnector (Vertx vertx, String address, Runnable disconnector, Gson gson)
	{
		super (vertx, gson, disconnector);
		this.ebus = vertx.eventBus ();
		this.address = address;
	}

	public EbusReactiveConnector (Vertx vertx, String address, Runnable disconnector)
	{
		this (vertx, address, disconnector, RequestResponseSerializer.GetDefaultGson ());
	}

	// -------------------------------------------------------------------
	public static <API extends ReactiveClient<API>> void connectToServer (String serviceName, Class<API> apiClass,
			Consumer<API> connectionHandler, Handler<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException
	{
		Constructor<API> apiConstructor = apiClass.getConstructor (IReactiveConnector.class);
		new AbstractPTCVerticle ()
		{
			@Override
			public void start () throws Exception
			{
				super.start ();
				findService (serviceName, this::onConnection, ex -> {
					terminate ();
					if (failHandler != null)
						failHandler.handle (ex);
				});
			}

			private void onConnection (JsonObject location)
			{
				String address = location.getString ("endpoint");
				try {
					API apiClient = apiConstructor
							.newInstance (new EbusReactiveConnector (vertx, address, this::terminate));
					vertx.executeBlocking (f -> connectionHandler.accept (apiClient), false, null);
					// connectionHandler.accept (apiClient);
				}
				catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					failHandler.handle (e);
				}
			}

			public void run ()
			{
				runClustered (this, buildVertxOptions ());
			}
		}.run ();
	}

	public static <API extends ReactiveClient<API>> void connectToServer (Class<API> apiClass,
			Consumer<API> connectionHandler, Handler<Throwable> failHandler)
			throws NoSuchMethodException, SecurityException
	{
		connectToServer (apiClass.getName (), apiClass, connectionHandler, failHandler);
	}

	// -------------------------------------------------------------------
	public static <API extends ReactiveClient<API>> CompletableFuture<API> connectToServer (String serviceName,
			Class<API> apiClass)
	{
		CompletableFuture<API> future = new CompletableFuture<API> ();
		try {
			connectToServer (serviceName, apiClass, api -> future.complete (api),
					ex -> future.completeExceptionally (ex));
		}
		catch (NoSuchMethodException | SecurityException e) {
			future.completeExceptionally (e);
		}
		return future;
	}

	public static <API extends ReactiveClient<API>> CompletableFuture<API> connectToServer (Class<API> apiClass)
	{
		return connectToServer (apiClass.getName (), apiClass);
	}

	// -------------------------------------------------------------------
	public static <API extends ReactiveClient<API>> Single<API> connectToServerRx (String serviceName,
			Class<API> apiClass)
	{
		return IReactiveConnector.futureToSingle ( () -> connectToServer (serviceName, apiClass));
	}

	// -------------------------------------------------------------------
	public static <API extends ReactiveClient<API>> Single<API> connectToServerRx (Class<API> apiClass)
	{
		return connectToServerRx (apiClass.getName (), apiClass);
	}

	@Override
	protected void publishRequest (String request)
	{
		ebus.publish (address, request);
	}

	@Override
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
			Consumer<ServiceResponse<RESPONSE>> handler, Function<String, ServiceResponse<RESPONSE>> reader)
	{
		ebus.send (address, request, res -> processResponse (res, handler, reader));
	}

	protected <RESPONSE extends JsonSerializable> void processResponse (AsyncResult<Message<Object>> res,
			Consumer<ServiceResponse<RESPONSE>> handler, Function<String, ServiceResponse<RESPONSE>> reader)
	{
		ServiceResponse<RESPONSE> response = null;
		if (res.succeeded ()) {
			Object responseBody = res.result ().body ();
			logInfo ("Received success response:" + responseBody);
			response = reader.apply (responseBody.toString ());
			response.setConnector (new EbusReplyReactiveConnector (vertx, res.result (), gson));
		}
		else {
			logInfo ("Received failed response:" + res.cause ());
			try {
				response = new BasicServiceResponse<RESPONSE> (res.cause ());
			}
			catch (Exception e) {
			}
		}
		handler.accept (response);
	}

	@Override
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
			Consumer<ServiceResponse<RESPONSE>> handler, Class<RESPONSE> responseCls)
	{
		ebus.send (address, request, res -> processResponse (res, handler, responseCls));
	}

	@SuppressWarnings ("unchecked")
	protected <RESPONSE extends JsonSerializable> void processResponse (AsyncResult<Message<Object>> res,
			Consumer<ServiceResponse<RESPONSE>> handler, Class<RESPONSE> responseCls)
	{
		processResponse (res, handler,
				body -> toResponse ((ServiceMessage<RESPONSE>) gson.fromJson (body, responseCls)));
	}

	@Override
	protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
			Consumer<ServiceResponse<RESPONSE>> handler, Type type)
	{
		logInfo ("Sending request: " + request);
		ebus.send (address, request, res -> processResponse (res, handler, type));
	}

	@SuppressWarnings ("unchecked")
	protected <RESPONSE extends JsonSerializable> void processResponse (AsyncResult<Message<Object>> res,
			Consumer<ServiceResponse<RESPONSE>> handler, Type type)
	{
		processResponse (res, handler, body -> toResponse ((ServiceMessage<RESPONSE>) gson.fromJson (body, type)));
	}

	// -------------------------------------------------------------------------------------
	private class EbusReplyReactiveConnector extends VertxReactiveConnector
	{
		private Message<Object> responseMessage;

		public EbusReplyReactiveConnector (Vertx vertx, Message<Object> responseMessage, Gson gson)
		{
			super (vertx, gson, null);
			this.responseMessage = responseMessage;
		}

		@Override
		protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
				Consumer<ServiceResponse<RESPONSE>> handler, Class<RESPONSE> responseCls)
		{
			responseMessage.reply (request, res -> processResponse (res, handler, responseCls));
		}

		@Override
		protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
				Consumer<ServiceResponse<RESPONSE>> handler, Type type)
		{
			responseMessage.reply (request, res -> processResponse (res, handler, type));
		}

		@Override
		protected <RESPONSE extends JsonSerializable> void sendRequest (String request,
				Consumer<ServiceResponse<RESPONSE>> handler, Function<String, ServiceResponse<RESPONSE>> reader)
		{
			responseMessage.reply (request, res -> processResponse (res, handler, reader));
		}

	}

}
