package com.ptc.ccp.vertxservices;

public class VServiceCommonData
{
	
	public static final String ServiceAnnounceAddress = "com.ptc.service-announce";
	
	public static final String EBusServiceAddress = "dd-event-bus";
	
	public static final String EBusReaderServiceName = "test.vertx.services.ebusreader";
	
}
