package com.ptc.ccp.vertxservices;

import com.ptc.ccp.vertx.utils.JsonTools;
import com.sm.javax.langx.Strings;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.types.HttpEndpoint;
import io.vertx.servicediscovery.types.MessageSource;

public abstract class TypedAbstractRestService<REQ_TYPE, RESP_TYPE> extends AbstractPTCVerticle
{

	public enum AccessMethod {
		EventBus, Web, Both
	};
	
	// --------------------------------------------------------------------------
	@SettingProperty (defaultValue="", cmdLineName="publicwebhost", envVariableName="PUBLIC_WEB_HOST",
			documentation="For web services running inside a container, use this host name insead of the internal host name/IP"
					+ " for service registration information.")
	protected static String PublicHttpHost;
	
	@SettingProperty (defaultValue="0", cmdLineName="publicwebport", envVariableName="PUBLIC_WEB_PORT",
			documentation="For web services running inside a container, use this port insead of the real HttpPort"
					+ " for service registration information.")
	protected static int PublicHttpPort;
	
	// --------------------------------------------------------------------------
	protected static final int    DefaultHttpPort = 8080;
	protected static final String DefaultEventBusAddress = "preferences-eventbus";
	protected static final String DefaultServerPath = "/preferences/mdb";
	protected static final String DefaultDebugPath = "/debug/*";
	protected static final String DeafultWebRoot = "www";
	protected static final String DeafultIndexPage = "index.html";
	
	protected static final String DefaultWebServiceName = "AbstractWebService";
	protected static final String DefaultEventbusServiceName = "AbstractEventbusService";

	
	protected AccessMethod method;
	protected int          port; 
	protected String       EventBusAddress;
	protected String       ServerPath;
	protected String       DebugPath;
	protected String       WebRoot;
	protected String       IndexPage;
	
	protected String       webServiceName;
	protected String       busServiceName;
	private JsonObject     serviceMetadata;

	private String webServiceAddress;
	
	public TypedAbstractRestService (AccessMethod method, int port)
	{
		super ();
		this.method = method;
		this.port = (port > 0 ? port : DefaultHttpPort);
		ApplicationProperties.processStaticProperties (AbstractRestService.class);
		//
		EventBusAddress = DefaultEventBusAddress;
		ServerPath = DefaultServerPath;
		DebugPath = DefaultDebugPath;
		WebRoot = DeafultWebRoot;
		IndexPage = DeafultIndexPage;
		webServiceName = null;
		busServiceName = null;
		serviceMetadata = null;
	}
	
	public TypedAbstractRestService (AccessMethod method)
	{
		this (method, DefaultHttpPort);
	}
	
	public TypedAbstractRestService (int port)
	{
		this (AccessMethod.Web, port);
	}
	
	// ------------------------------------------------------------------
	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setEventBusAddress (String address)
	{
		EventBusAddress = address;
		return this;
	}
	
	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setServerPath (String path)
	{
		ServerPath = path;
		return this;
	}

	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setDebudPath (String path)
	{
		DebugPath = path;
		return this;
	}
	
	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setWebRoot (String path)
	{
		WebRoot = path;
		return this;
	}
	
	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setIndexPage (String path)
	{
		IndexPage = path;
		return this;
	}
	
	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setWebServiceName (String webServiceName)
	{
		this.webServiceName = webServiceName;
		return this;
	}

	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setBusServiceName (String busServiceName)
	{
		this.busServiceName = busServiceName;
		if (Strings.isEmpty (EventBusAddress) || EventBusAddress.equals (DefaultEventBusAddress)) {
			if (! Strings.isEmpty (busServiceName))
			EventBusAddress = busServiceName;
		}
		return this;
	}
	
	public TypedAbstractRestService<REQ_TYPE, RESP_TYPE> setServiceMetadata (JsonObject serviceMetadata)
	{
		this.serviceMetadata = serviceMetadata;
		return this;
	}
	
	public void addAccessMethod (AccessMethod method)
	{
		if (this.method == null) {
			this.method = method;
		}
		else if (this.method != method) {
			this.method = AccessMethod.Both;
		}
	}
	
	public void setWebPort (int port)
	{
		addAccessMethod (AccessMethod.Web);
		this.port = (port > 0 ? port : DefaultHttpPort);
	}
	
	// ------------------------------------------------------------------
	protected void onStart ()
	{
		initOnStart ();
		
		if (method == AccessMethod.EventBus || method == AccessMethod.Both) {
			startEventBusService ();
		}
		if (method == AccessMethod.Web || method == AccessMethod.Both) {
			startWebService ();
		}
		
		onStartComplete ();
	}
	
	@Override
	public void start () throws Exception
	{
		super.start ();
		onStart ();
	}
	
	@Override
	protected void onStop (boolean terminatedNormally)
	{
		super.onStop (terminatedNormally);
		unpublishService ();
	}

	// ------------------------------------------------------------------
	protected void startEventBusService ()
	{
		logInfo ("Starting event-bus service on " + EventBusAddress);
		EventBus ebus = vertx.eventBus ();
		ebus.consumer (EventBusAddress, this::processRequest);
		logInfo ("Service will consume messages from " + EventBusAddress);
		//
		if (! Strings.isEmpty (getBusServiceName ())) {
			logInfo ("Registering event-bus service '" + getBusServiceName () + "'");
			Record record = MessageSource.createRecord (getBusServiceName (), EventBusAddress, this.getClass (), serviceMetadata);
			/*
			 * This is a fix. for some reason Messagesource.createRecord does NOT include metadata object in the record
			 */
			if (serviceMetadata != null && serviceMetadata.size () > 0) {
				JsonObject metadata = record.getMetadata ();
				serviceMetadata.forEach (entry -> metadata.put (entry.getKey (), entry.getValue ()));
				record.setMetadata (metadata);
			}
			publishService (record);
		}
		logInfo ("Event-bus service started.");
	}
	
	protected HttpServerOptions buildHttpServerOptions ()
	{
		return new HttpServerOptions ();
	}
	
	protected void appendWebService (Router router)
	{
		// empty
	}
	
	protected void startWebService ()
	{
		logInfo ("Starting web service ...");
		Router router = Router.router (vertx);
		router.route (ServerPath).handler (BodyHandler.create ());
		router.route (ServerPath).handler (this::processHttpRequest);
		if (DebugPath != null) {
			StaticHandler staticHandler = StaticHandler.create ().setWebRoot (WebRoot).setIndexPage (IndexPage);
			router.route (DebugPath).handler (context -> staticHandler.handle (context));
		}
		//
		appendWebService (router);
		//
		HttpServer server = vertx.createHttpServer (buildHttpServerOptions ());
		server.requestHandler (router::accept).listen (port);
		
		logInfo ("Public HTTP host: " + PublicHttpHost);
		String hname = (! Strings.isEmpty (PublicHttpHost)) ? PublicHttpHost : hostname ();
		webServiceAddress = "http://" + hname + ":" + server.actualPort () + ServerPath;
		logInfo ("Web service started on http://" + hname + ":" + server.actualPort () + ServerPath);
		int extWebPort = (PublicHttpPort > 0 ? PublicHttpPort : server.actualPort ());
		if (! Strings.isEmpty (PublicHttpHost)) {
			logInfo ("Public web service started on http://" + hname + ":" + extWebPort + ServerPath);
		}
		if (DebugPath != null) {
			String debugPage = DebugPath;
			if (debugPage.endsWith ("*")) {
				debugPage = debugPage.substring (0, debugPage.length () - 1);
			}
			if (IndexPage != null) {
				if (! Strings.isEmpty (ServerPath)) {
					debugPage += IndexPage + "?action=" + ServerPath;
				}
			}
			logInfo ("Web service debug is on http://" + hname + ":" + server.actualPort () + debugPage);
			if (! Strings.isEmpty (PublicHttpHost)) {
				logInfo ("Public web service debug is on http://" + hname + ":" + extWebPort + debugPage);
			}
		}
		//
		if (! Strings.isEmpty (getWebServiceName ())) {
			int port = server.actualPort ();
			if (! Strings.isEmpty (PublicHttpHost)) {
				hname = PublicHttpHost;
				port = extWebPort;
			}
			int publicPort = (PublicHttpPort > 0) ? PublicHttpPort : port;
			logInfo ("PublicHttpHost : " + PublicHttpHost);
			logInfo ("publicPort     : " + publicPort);
			Record record = HttpEndpoint.createRecord (getWebServiceName (), hname, publicPort, ServerPath, serviceMetadata);
			publishService (record);
		}
		logInfo ("Web service started");
	}
	
	// --------------------------------------------------------------------------------
	protected void processHttpRequest (RoutingContext routingContext)
	{
		REQ_TYPE command = buildCommand (routingContext);
		executeRequest (command, result -> {
			HttpServerResponse response = routingContext.response ();
			response.putHeader ("Content-Type", "text/javascript;charset=ISO-8859-1");
			response.end (result.toString ());
			logResponse (result);
		});
	}
	
	protected void logResponse (RESP_TYPE result)
	{
		if (getLogger () != null) {
			getLogger ().info ("Response sent: " + result.toString ());
		}
	}

	protected Object parseValue (Object value)
	{
		Object res = value;
		if (value != null) {
			if (value instanceof String) {
				String vstr = value.toString ();
				boolean converted = false;
				if (Strings.isQuoted (vstr) || Strings.isQuoted (vstr, "'")) {
					res  = Strings.unQuote (vstr, false);
					converted = true;
				}
				// a) try to convert to JSON
				if (! converted) {
					try {
						res = new JsonObject (vstr);
						converted = true;
					}
					catch (Throwable ex) {
					}
				}
				if (! converted) {
					try {
						res = new JsonArray (vstr);
						converted = true;
					}
					catch (Throwable ex) {
					}
				}
				// b) try to convert to integer
				if (! converted) {
					try {
						res = Integer.valueOf (vstr);
						converted = true;
					}
					catch (NumberFormatException ex) {
					}
				}
				// b) try to convert to boolean
				if (! converted && (vstr.equals ("true") || vstr.equals ("false"))) {
					res  = Boolean.valueOf (vstr);
					converted = true;
				}
				// c) try to convert to double
				if (! converted) {
					try {
						res  = Double.valueOf (vstr);
						converted = true;
					}
					catch (NumberFormatException ex) {
						res = value;
					}
				}
			}
		}
		return res;
	}

	protected void processRequest (Message<Object> msg)
	{
		logInfo ("Server got request: " + msg.body ());
		REQ_TYPE request = buildCommand (msg);
		executeRequest (request, result -> {
			logInfo ("Sending response: " + result);
			msg.reply (result, this::processResultReply);
			logResponse (result);
		});
	}
	
    protected void processResultReply (AsyncResult<Message<Object>> res)
    {
           if (res.succeeded ()) {
                  Message<Object> msg = res.result ();
                  processRequest (msg);
           }
    }

    /*
	 * converts HTTP request parameters to proper type command
	 */
	protected abstract REQ_TYPE buildCommand (RoutingContext routingContext);
	protected abstract REQ_TYPE buildCommand (Message<Object> msg);
	
	protected abstract void initOnStart ();
	
	protected void onStartComplete () {}

	protected abstract void executeRequest (REQ_TYPE cmd, Handler<RESP_TYPE> handler);
	
	public String getWebServiceName () { return webServiceName; }
	public String getBusServiceName () { return busServiceName; }
	
	public String getWebServiceAddress () { return webServiceAddress; }

	protected JsonObject buildSuccess (String msg)
	{
		return JsonTools.buildSuccess (msg);
	}
	
	protected JsonObject buildSuccess ()
	{
		return buildSuccess (null);
	}

	
	protected JsonObject buildFail (String msg)
	{
		return JsonTools.buildFail (msg);
	}

}
