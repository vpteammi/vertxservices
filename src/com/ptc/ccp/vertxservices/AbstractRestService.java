package com.ptc.ccp.vertxservices;

import java.util.function.BiConsumer;

import com.sm.javax.langx.Strings;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public abstract class AbstractRestService extends JsonRestService 
{

	public AbstractRestService (AccessMethod method, int port)
	{
		super (method, port);
	}
	
	public AbstractRestService (AccessMethod method)
	{
		this (method, DefaultHttpPort);
	}
	
	// --------------------------------------------------------------------------
	public static class CommandArgument
	{
		private String  argumentName;
		private String description;
		private boolean optional;
		
		public CommandArgument (String argumentName, String description)
		{
			this (argumentName, description, false);
		}
	
		public CommandArgument (String argumentName, String description, boolean optional)
		{
			super ();
			this.argumentName = argumentName;
			this.optional = optional;
			this.description = description;
		}
	
		public String getArgumentName ()
		{
			return argumentName;
		}
	
		public String getDescription ()
		{
			return Strings.concat (description, " ", isOptional () ? "(optional)" : null);
		}
	
		public boolean isOptional ()
		{
			return optional;
		}
		
		public JsonObject toJson ()
		{
			JsonObject jsobj = new JsonObject ();
			jsobj.put ("argument-name", getArgumentName ());
			String descr = getDescription ();
			if (! Strings.isEmpty (descr)) {
				jsobj.put ("argument-description", descr);
			}
			jsobj.put ("optional", optional);
			return jsobj;
		}
	}

	// =====================================================================================
	public static class RequestAction
	{
		private String cmdName;
		private BiConsumer<JsonObject, Handler<JsonObject>> cmdProcessor;
		private String descrption;
		private AbstractRestService.CommandArgument[] cmdArgs;
		
		public RequestAction (String cmdName, BiConsumer<JsonObject, Handler<JsonObject>> cmdProcessor, String documentation, AbstractRestService.CommandArgument[] cmdArgs)
		{
			super ();
			this.cmdName = cmdName;
			this.cmdProcessor = cmdProcessor;
			this.cmdArgs = cmdArgs;
			this.descrption = documentation;
		}

		public void processRequest (JsonObject req, Handler<JsonObject> handler)
		{
			cmdProcessor.accept (req, handler);
		}

		public JsonObject toJson ()
		{
			JsonObject jsobj = new JsonObject ();
			jsobj.put ("command", cmdName);
			if (! Strings.isEmpty (descrption)) {
				jsobj.put ("descrption", descrption);
			}
			if (cmdArgs != null && cmdArgs.length > 0) {
				JsonArray args = new JsonArray ();
				for (AbstractRestService.CommandArgument arg : cmdArgs) {
					args.add (arg.toJson ());
				}
				jsobj.put ("arguments", args);
			}
			return jsobj;
		}
	}
	

}
