package com.ptc.ccp.vertxservices;

import io.vertx.core.eventbus.Message;
import io.vertx.ext.web.RoutingContext;

public abstract class StringRestService extends TypedAbstractRestService<String, String>
{

	public StringRestService (com.ptc.ccp.vertxservices.TypedAbstractRestService.AccessMethod method, int port)
	{
		super (method, port);
	}

	public StringRestService (com.ptc.ccp.vertxservices.TypedAbstractRestService.AccessMethod method)
	{
		super (method);
	}

	public StringRestService (int port)
	{
		super (port);
	}

	// ------------------------------------------------------------------------------------------
	@Override
	protected String buildCommand (RoutingContext routingContext)
	{
		return routingContext.getBodyAsString ();
	}
	
	@Override
	protected String buildCommand (Message<Object> msg)
	{
		Object body = msg.body ();
		logDebug ("Received message: " + body);
		String request = null;
		if (body != null) {
			request = body.toString ();
		}
		return request;
	}
	
	// -------------------------------------------------------------------------------
	// To be defined in sub classes
	
	// @Override protected void initOnStart ();

	// @Override protected void executeRequest (String cmd, Handler<String> handler);

}
