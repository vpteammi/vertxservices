package com.ptc.ccp.vertxservices;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public abstract class JsonRestService extends TypedAbstractRestService<JsonObject, JsonObject>
{

	public JsonRestService (TypedAbstractRestService.AccessMethod method, int port)
	{
		super (method, port);
	}
	
	public JsonRestService (TypedAbstractRestService.AccessMethod method)
	{
		super (method);
	}
	
	public JsonRestService (int port)
	{
		super (port);
	}

	// -------------------------------------------------------------------------------
	/*
	 * converts HTTP request parameters to JSON command
	 * Actual format of JSON command depends on implementation of executeRequest (...)
	 * NB: Override this method in your sub class in order to convert GET method
	 *     arguments to your JSON request
	 */
	protected JsonObject buildJsonCommand (RoutingContext routingContext)
	{
		JsonObject json = new JsonObject ();
		String body = routingContext.getBodyAsString ();
		logDebug ("Received message: " + body);
		if (body != null && body.length () > 0) {
			try {
				JsonObject jsobj = new JsonObject (body);
				json.forEach (entry -> {
					logDebug ("POST Entry: " + entry.getKey () + " = " + entry.getValue ());
					jsobj.put (entry.getKey (), entry.getValue ());
				});
				json = jsobj;
			}
			catch (Exception ex) {
				logError ("Failed to convert message body to JSON.", ex);
			}
		}
		return json;
	}

	@Override
	protected JsonObject buildCommand (RoutingContext routingContext)
	{
		return buildJsonCommand (routingContext);
	}

	@Override
	protected JsonObject buildCommand (Message<Object> msg)
	{
		Object body = msg.body ();
		logDebug ("Received message: " + body);
		JsonObject request = null;
		if (body instanceof JsonObject) {
			request = (JsonObject) body;
		}
		else {
			request = new JsonObject (body.toString ());
		}
		return request;
	}

	// -------------------------------------------------------------------------------
	// To be defined in sub classes
	
	// @Override protected void initOnStart ();

	// @Override protected void executeRequest (JsonObject cmd, Handler<JsonObject> handler);

}
