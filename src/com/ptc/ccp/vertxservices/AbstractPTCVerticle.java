package com.ptc.ccp.vertxservices;

import static java.net.InetAddress.getLocalHost;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.hazelcast.config.Config;
import com.hazelcast.config.InterfacesConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.ptc.ccp.aws.utils.AWSMetadataReader;
import com.ptc.ccp.vertx.utils.VertxContextScheduler;
import com.ptc.ccp.vertxservices.reactive.VertxReactiveFramework;
import com.sm.javax.langx.Strings;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.netx.NetUtilities;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.CommandLineArguments;
import com.sm.javax.utilx.TimeUtils;

import io.reactivex.Scheduler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public abstract class AbstractPTCVerticle extends AbstractVerticle
{
	@SettingProperty (defaultValue = "true", cmdLineName = "usedefaultclusterhost", envVariableName = "USE_DEFAULT_CLUSTER_HOST", documentation = "Use current IP address as the cluster host.")
	private static boolean UseDefaultClusterHost;

	@SettingProperty (defaultValue = "", cmdLineName = "clusterhost", envVariableName = "CLUSTER_HOST", documentation = "IP address of the cluster host. Set it to machine IP address")
	private static String ClusterHost;

	@SettingProperty (defaultValue = "0", cmdLineName = "clusterport", envVariableName = "CLUSTER_PORT", documentation = "Specify cluster port.")
	private static int ClusterPort;

	@SettingProperty (defaultValue = "", cmdLineName = "clusterpublichost", envVariableName = "CLUSTER_PUBLIC_HOST", documentation = "IP address of the cluster public host. Set it to machine IP address")
	private static String ClusterPublicHost;

	@SettingProperty (defaultValue = "0", cmdLineName = "clusterpublicport", envVariableName = "CLUSTER_PUBLIC_PORT", documentation = "Specify cluster public port.")
	private static int ClusterPublicPort;

	@SettingProperty (defaultValue = "0", cmdLineName = "hazelcastclusterport", envVariableName = "HAZELCAST_CLUSTER_PORT", documentation = "Specify hazelcast cluster port")
	private static int HazelcastClusterPort;

	@SettingProperty (defaultValue = "", cmdLineName = "clustermemberlist", envVariableName = "CLUSTER_MEMBER_LIST", documentation = "List of Cluster Members")
	private static String ClusterMemberList;

	@SettingProperty (defaultValue = "false", cmdLineName = "isAWSDeployment", envVariableName = "IS_AWS_DEPLOYMENT", documentation = "Is the service to be deployed on AWS Instance.")
	private static boolean IsASWDeployment;

	@SettingProperty (defaultValue = "false", cmdLineName = "serviceStdDebugOutput", documentation = "Copy DEBUG logging to STDOUT")
	private static boolean StdDebugOutputOn;

	@SettingProperty (defaultValue = "true", cmdLineName = "serviceStdInfoOutput", documentation = "Copy INFO logging to STDOUT")
	private static boolean StdInfoOutputOn;

	@SettingProperty (defaultValue = "true", cmdLineName = "serviceStdErrorOutput", documentation = "Copy ERROR logging to STDOUT")
	private static boolean StdErrorOutputOn;

	@SettingProperty (defaultValue = "false", envVariableName = "HAZELCAST_ADD_PUBLIC_NET_INTERFACE", documentation = "Add cluster public host (if set) to list of network interfaces")
	private static boolean HazelcastAddPublicNetInterface;

	@SettingProperty (defaultValue = "true", cmdLineName = "insidehazelcast", envVariableName = "USE_INSIDE_HAZELCAST", documentation = "Cancel usage of in-code Hazelcust configuration and use cluster.xml file if provided with"
			+ " -Dvertx.hazelcast.config=<file_path>\\cluster.xml")
	private static boolean UseHazelcastConfiguration;

	@SettingProperty (defaultValue = "true", cmdLineName = "staticlogging", documentation = "Turn on/off static logging (default ON)")
	private static boolean StaticLoggingOn;

	@SettingProperty (defaultValue = "10", envVariableName = "HAZELCAST_MAX_NO_HEARTBEAT_SECONDS", documentation = "Value for hazelcast.max.no.heartbeat.seconds (in seconds)")
	private static int hazelcast_max_no_heartbeat_seconds;

	@SettingProperty (defaultValue = "true", envVariableName = "UNDEPLOY_VERTICLES_ON_HAZLECAST_SHUTDOWN", documentation = "Set on shutdown listener for hazelcast and undeplay verticles if called.")
	private static boolean UndeployVerticlesOnHazlecastShutdown;

	// --------------------------------------------------------------------------------------------------------------
	private final static ConcurrentHashMap<String, AbstractPTCVerticle> ActiveVerticles = new ConcurrentHashMap<> ();
	private static boolean shutdownHookSet = false;

	static private synchronized void setShutdownHook ()
	{
		if (!shutdownHookSet) {
			Runtime.getRuntime ().addShutdownHook (new Thread ()
			{
				public void run ()
				{
					System.out.println ("AbstractPTCVerticle.ShutdownHook gets invoked");
					for (AbstractPTCVerticle verticle : ActiveVerticles.values ()
							.toArray (new AbstractPTCVerticle [ActiveVerticles.size ()])) {
						verticle.terminate (false /* terminated abnormally */);
					}
				}
			});
			shutdownHookSet = true;
		}
	}

	// --------------------------------------------------------------------------------------------------------------

	private HashMap<String, Record> services;
	protected ServiceDiscovery discovery;
	private HashSet<Record> records;
	protected boolean skipServiceRegistration;

	protected AbstractPTCVerticle ()
	{
		super ();
		ApplicationProperties.processStaticProperties (AbstractPTCVerticle.class);
		this.skipServiceRegistration = false;
		setShutdownHook ();
		/*
		 * Runtime.getRuntime ().addShutdownHook (new Thread () { public void
		 * run () { logDebug ("AbstractPTCVerticle.ShutdownHook gets invoked");
		 * terminate (false /* terminated abnormally * /); } });
		 */
	}

	@Override
	public void stop () throws Exception
	{
		onStop (true);
		super.stop ();
	}

	public AbstractPTCVerticle skipServiceRegistration (boolean val)
	{
		skipServiceRegistration = val;
		return this;
	}

	public AbstractPTCVerticle skipServiceRegistration ()
	{
		return skipServiceRegistration (true);
	}

	// --------------------------------------------------------------------------------------------------------------
	protected void onStop (boolean terminatedNormally)
	{
		// intentionally left empty
	}

	// --------------------------------------------------------------------------------------------------------------
	protected ServiceDiscovery getServiceDiscovery ()
	{
		if (discovery == null) {
			ServiceDiscoveryOptions dops = new ServiceDiscoveryOptions ()
					.setAnnounceAddress (VServiceCommonData.ServiceAnnounceAddress).setName ("ptc-drive");
			discovery = ServiceDiscovery.create (vertx, dops);
			services = new HashMap<> ();
		}
		return discovery;
	}

	protected void publishService (String type, JsonObject location)
	{
		Record record = new Record ().setType (type).setName (this.getClass ().getName ()).setLocation (location);
		publishService (record);
	}

	protected void publishService (Record record)
	{
		if (skipServiceRegistration) {
			return;
		}
		if (discovery == null) {
			logInfo ("Getting Service Discovery");
			ServiceDiscoveryOptions dops = new ServiceDiscoveryOptions ()
					.setAnnounceAddress (VServiceCommonData.ServiceAnnounceAddress).setName ("ptc-drive");
			discovery = ServiceDiscovery.create (vertx, dops);
			services = new HashMap<> ();
			records = new HashSet<> ();
		}
		records.add (record);

		logInfo ("Trying to publish service: " + recordToString (record));
		discovery.publish (record, res -> {
			if (res.succeeded ()) {
				logInfo ("Service published: " + recordToString (res.result ()));
				services.put (record.getName (), res.result ());
			}
			else {
				logError ("Failed to publish service", res.cause ());
			}
		});
	}

	protected void unpublishService (String type)
	{
		if (services != null) {
			Record record = services.get (type);
			if (record != null) {
				discovery.unpublish (record.getRegistration (), res -> {
					if (res.succeeded ()) {
						logDebug ("Service unpublished: " + res.result ());
						services.remove (type);
					}
					else {
						logDebug ("Failed to unpublish service: " + res.cause ());
					}
				});
			}
		}
	}

	public static String recordToString (Record record)
	{
		return record.toJson ().toString ();
	}

	protected void unpublishService ()
	{
		logDebug ("Unregistering services (if any)");
		if (records != null && discovery != null) {
			for (Record record : records) {
				unpublishService (record);
			}
			records.clear ();
		}
	}

	protected void unpublishService (Record record)
	{
		discovery.unpublish (record.getRegistration (), res -> {
			if (res.succeeded ()) {
				logInfo ("Service unpublished: " + recordToString (record));
			}
			else {
				logError ("Failed to unpublish service", res.cause ());
			}
		});
	}

	public Scheduler scheduler ()
	{
		return VertxContextScheduler.scheduler (vertx);
	}

	// -------------------------------------------------------------------------------------
	public void findServices (Function<Record, Boolean> filter, Handler<AsyncResult<List<Record>>> handler)
	{
		getServiceDiscovery ().getRecords (filter, handler);
	}

	public void findServiceRecords (JsonObject servicePattern, Handler<List<Record>> successHandler,
			Handler<Throwable> failHandler)
	{
		getServiceDiscovery ().getRecords (servicePattern, res -> {
			if (res.succeeded ()) {
				successHandler.handle (res.result ());
			}
			else {
				failHandler.handle (res.cause ());
			}
		});
	}

	public void findServices (JsonObject servicePattern, Handler<List<JsonObject>> successHandler,
			Handler<Throwable> failHandler)
	{
		getServiceDiscovery ().getRecords (servicePattern, res -> {
			if (res.succeeded ()) {
				List<Record> records = res.result ();
				if (records != null && records.size () > 0) {
					List<JsonObject> recs = records.stream ().map (rec -> rec.getLocation ())
							.collect (Collectors.toList ());
					vertx.executeBlocking (future -> {
						successHandler.handle (recs);
						future.complete ();
					}, ar -> {
					});
				}
				else {
					failHandler.handle (null);
				}
			}
			else {
				failHandler.handle (res.cause ());
			}
		});
	}

	public void findServices (String serviceName, Handler<List<JsonObject>> successHandler,
			Handler<Throwable> failHandler)
	{
		JsonObject servicePattern = new JsonObject ().put ("name", serviceName);
		findServices (servicePattern, successHandler, failHandler);
	}

	public void findServices (String serviceName, Handler<List<JsonObject>> successHandler)
	{
		findServices (serviceName, successHandler, ex -> {
			if (ex == null) {
				logError ("No services with name '" + serviceName + "' found - shutting down");
			}
			else {
				logError ("Service discovery failed: " + ex + "\n  - shutting down", ex);
			}
			vertx.close ();
		});
	}

	public void findService (JsonObject servicePattern, Handler<JsonObject> successHandler,
			Handler<Throwable> failHandler)
	{
		getServiceDiscovery ().getRecords (servicePattern, res -> {
			if (res.succeeded ()) {
				List<Record> records = res.result ();
				if (records != null && records.size () > 0) {
					Record rec = records.get (0);
					logInfo ("Service discovered: " + recordToString (rec));
					JsonObject location = rec.getLocation ();
					onServiceFound (location);
					vertx.executeBlocking (future -> {
						successHandler.handle (location);
						future.complete ();
					}, ar -> {
						logDebug ("OK");
					});
				}
				else {
					failHandler
							.handle (new Exception ("No service for pattern " + servicePattern.encode () + " found"));
				}
			}
			else {
				failHandler.handle (res.cause ());
			}
		});
	}

	protected void onServiceFound (JsonObject location)
	{
		// do nothing
		// vertx.eventBus ().send (location.getString ("endpoint"), new
		// JsonObject ().put ("command", "ping"));
	}

	public void findService (String serviceName, Handler<JsonObject> successHandler, Handler<Throwable> failHandler)
	{
		findService (new JsonObject ().put ("name", serviceName), successHandler, failHandler);
	}

	public void findService (String serviceName, Handler<JsonObject> handler)
	{
		findService (serviceName, handler, ex -> {
			if (ex == null) {
				logError ("No services with name '" + serviceName + "' found - shutting down");
			}
			else {
				logError ("Service discovery failed: " + ex + "\n  - shutting down", ex);
			}
			vertx.close ();
		});
	}

	// -------------------------------------------------------------------------------------
	protected void terminate (boolean terminatedNormally)
	{
		String id = deploymentID ();
		try {
			logInfo ("Terminating verticle " + id);
			onStop (terminatedNormally);
			CompletableFuture<Throwable> waitForUndeploy = new CompletableFuture<> ();
			vertx.undeploy (id, result -> {
				if (result.succeeded ()) {
					ActiveVerticles.remove (id);
				}
				logInfo ("Verticle undeployed " + id + "=" + result.succeeded ());
				waitForUndeploy.complete (result.cause ());
			});
		}
		catch (Throwable ex) {
			if (ActiveVerticles.size () < 2) {
				logInfo ("Terminating completetly");
				System.exit (0);
			}
			logError ("Failed to terminate virticle " + id, ex);
		}
	}

	protected void terminate ()
	{
		terminate (true);
	}

	protected Logger getLogger ()
	{
		return null;
	}

	protected void logInfo (String msg)
	{
		Logger logger = getLogger ();
		if (logger != null) {
			logger.info (msg);
		}
		if (StdInfoOutputOn) {
			System.out.println (TimeUtils.logLine (msg));
		}
	}

	protected void logDebug (String msg)
	{
		Logger logger = getLogger ();
		if (logger != null) {
			logger.debug (msg);
		}
		if (StdDebugOutputOn) {
			System.out.println (TimeUtils.logLine (msg));
		}
	}

	protected void logError (String msg)
	{
		Logger logger = getLogger ();
		if (logger != null) {
			logger.error (msg);
		}
		if (StdErrorOutputOn) {
			System.out.println (TimeUtils.logLine (msg));
		}
	}

	protected void logError (String msg, Throwable err)
	{
		Logger logger = getLogger ();
		if (logger != null) {
			logger.error (msg, err);
		}
		if (StdErrorOutputOn) {
			System.out.println (TimeUtils.logLine (msg + ": " + err.getMessage ()));
		}
	}

	// -------------------------------------------------------------------------------------
	public static void runClustered (Verticle serviceInstance, VertxOptions vops, Handler<AsyncResult<String>> handler)
	{
		vops = (vops == null ? new VertxOptions () : vops).setClustered (true);
		runService (serviceInstance, vops, handler);
	}

	public static void runClustered (Verticle serviceInstance, Handler<AsyncResult<String>> handler)
	{
		VertxOptions vops = new VertxOptions ().setClustered (true);
		runService (serviceInstance, vops, handler);
	}

	public static void runClustered (Verticle serviceInstance, VertxOptions vops)
	{
		vops = (vops == null ? new VertxOptions () : vops).setClustered (true);
		runService (serviceInstance, vops, null);
	}

	public static void runClustered (Verticle serviceInstance)
	{
		VertxOptions vops = new VertxOptions ().setClustered (true);
		runService (serviceInstance, vops, null);
	}

	public static void runHighlyAvailable (Verticle serviceInstance, VertxOptions vops,
			Handler<AsyncResult<String>> handler)
	{
		if (vops == null) {
			vops = new VertxOptions ();
		}
		vops.setClustered (true).setHAEnabled (true);
		runService (serviceInstance, vops, handler);
	}

	public static void runHighlyAvailable (Verticle serviceInstance, VertxOptions vops)
	{
		runHighlyAvailable (serviceInstance, vops, null);
	}

	public static void runHighlyAvailable (Verticle serviceInstance, Handler<AsyncResult<String>> handler)
	{
		VertxOptions vops = new VertxOptions ().setClustered (true).setHAEnabled (true);
		runService (serviceInstance, vops, handler);
	}

	public static void runHighlyAvailable (Verticle serviceInstance)
	{
		runHighlyAvailable (serviceInstance, null, null);
	}

	public static void runService (Verticle serviceInstance, VertxOptions vops, Handler<AsyncResult<String>> handler)
	{
		String message = " service " + serviceInstance.getClass ().getName () + "\n with " + vops;
		logStaticInfo ("Starting" + message);
		try {
			Vertx vertxInstance = VertxReactiveFramework.getVertxInstance (vops);
			if (vertxInstance != null) {
				vertxInstance.deployVerticle (serviceInstance, result -> {
					if (result.succeeded () && serviceInstance instanceof AbstractPTCVerticle) {
						AbstractPTCVerticle ptcv = (AbstractPTCVerticle) serviceInstance;
						ActiveVerticles.put (ptcv.deploymentID (), ptcv);
					}
					if (handler != null) {
						handler.handle (result);
					}
				});
			}
		}
		catch (Throwable t) {
			System.err.println ("Failed to start " + message);
			t.printStackTrace (System.err);
		}
	}

	public static String hostname ()
	{
		try {
			InetAddress localhost = getLocalHost ();
			String canHostname = localhost.getCanonicalHostName ();
			return (canHostname != null ? canHostname.toLowerCase () : localhost.getHostName ());
		}
		catch (final UnknownHostException e) {
			return "localhost";
		}
	}

	protected static CommandLineArguments parseArguments (String[] args)
	{
		return (new CommandLineArguments (args));
	}

	private static VertxOptions vertxOptions;

	public static VertxOptions buildVertxOptions ()
	{
		if (vertxOptions == null) {
			ApplicationProperties.processStaticProperties (AbstractPTCVerticle.class);
			vertxOptions = new VertxOptions ();

			String host = getClusterHost ();
			if (!Strings.isEmpty (host)) {
				vertxOptions.setClusterHost (host);
			}
			if (ClusterPort > 0) {
				vertxOptions.setClusterPort (ClusterPort);
			}

			String publicHost = getClusterPublicHost ();
			if (!Strings.isEmpty (publicHost)) {
				vertxOptions.setClusterPublicHost (publicHost);
			}
			if (ClusterPublicPort > 0) {
				vertxOptions.setClusterPublicPort (ClusterPublicPort);
			}

			logStaticInfo ("cluster-host = " + host);
			logStaticInfo ("cluster-port = " + ClusterPort);
			logStaticInfo ("cluster-public-host = " + publicHost);
			logStaticInfo ("cluster-public-port = " + ClusterPublicPort);
			logStaticInfo ("hazelcast-cluster-port = " + HazelcastClusterPort);

			if (UseHazelcastConfiguration) {
				Config hazelcastConfig = getHazelcastConfiguration (host, publicHost, HazelcastClusterPort);
				ClusterManager clusterManager = new HazelcastClusterManager (hazelcastConfig);
				vertxOptions.setClusterManager (clusterManager);
			}

			vertxOptions.setClustered (true);
		}

		return vertxOptions;
	}

	public static void logStaticInfo (String msg)
	{
		if (StaticLoggingOn) {
			System.out.println (msg);
		}
	}

	private static Config getHazelcastConfiguration (String clusterHost, String clusterPublicHost,
			int hazelcastClusterPort)
	{
		Config hazelcastConfig = new Config ();
		hazelcastConfig.setProperty ("hazelcast.max.no.heartbeat.seconds",
				Integer.toString (hazelcast_max_no_heartbeat_seconds));
		NetworkConfig networkConfig = hazelcastConfig.getNetworkConfig ();

		// Disable Multicast
		JoinConfig join = networkConfig.getJoin ();
		join.getMulticastConfig ().setEnabled (false);

		// Enable TcpIp
		TcpIpConfig tcpIpConfig = join.getTcpIpConfig ();
		tcpIpConfig.setEnabled (true);

		Collection<String> members = getClusterMemebers (clusterHost, clusterPublicHost);
		logStaticInfo ("Cluster Members : " + members);
		if (!members.isEmpty ()) {
			for (String member : members) {
				tcpIpConfig.addMember (member);
			}
		}

		// Set Public Address
		String publicAddress = getClusterPublicAddress (clusterPublicHost, hazelcastClusterPort);
		logStaticInfo ("Public Address : " + publicAddress);
		if (!Strings.isEmpty (publicAddress)) {
			networkConfig.setPublicAddress (publicAddress);
		}

		if (HazelcastAddPublicNetInterface && !Strings.isEmpty (clusterPublicHost)) {
			InterfacesConfig networkInterface = networkConfig.getInterfaces ();
			networkInterface.setEnabled (true).addInterface (clusterPublicHost);
		}

		if (!Strings.isEmpty (clusterHost)) {
			hazelcastConfig.setProperty ("hazelcast.local.localAddress", clusterHost);
			hazelcastConfig.setProperty ("hazelcast.socket.server.bind.any", "false");
			hazelcastConfig.setProperty ("hazelcast.socket.client.bind", "false");
		}

		return hazelcastConfig;
	}

	/**
	 * Get list of Cluster Members
	 * 
	 * For now we are returning members as clusterHost / clusterPublicHost But
	 * we can use 1. Command line parameter in argument list 2. Inject Member
	 * List using some logic. Like in DCOS setup we can collect all DCOS agents.
	 * 
	 * @return cluster members
	 */
	private static Collection<String> getClusterMemebers (String clusterHost, String clusterPublicHost)
	{
		Collection<String> members = new ArrayList<> ();
		if (!Strings.isEmpty (clusterPublicHost)) {
			members.add (clusterPublicHost);
		}
		else if (!Strings.isEmpty (clusterHost)) {
			members.add (clusterHost);
		}

		if (!Strings.isEmpty (ClusterMemberList)) {
			members = Arrays.asList (ClusterMemberList.split ("\\b*,\\b*"));
		}
		return members;
	}

	private static String getClusterHost ()
	{
		String host = ClusterHost;
		if (UseDefaultClusterHost) {
			try {
				InetAddress inetAddr = NetUtilities.getInetAddress ();
				if (inetAddr != null) {
					host = inetAddr.getHostAddress ();
				}
				else {
					host = InetAddress.getLocalHost ().getHostAddress ();
				}
			}
			catch (UnknownHostException e) {
				// ignore
			}
		}

		return host;
	}

	private static String getClusterPublicHost ()
	{
		String publicHost = null;
		logStaticInfo ("IsASWDeployment : " + IsASWDeployment);
		if (IsASWDeployment) {
			publicHost = AWSMetadataReader.getAWSInstancePrivateIP (StaticLoggingOn);
			if (Strings.isEmpty (publicHost)) {
				publicHost = ClusterPublicHost;
			}
		}
		else {
			publicHost = ClusterPublicHost;
		}
		return publicHost;
	}

	private static String getClusterPublicAddress (String clusterPublicHost, int hazelcastClusterPort)
	{
		String publicAddress = "";
		if (!Strings.isEmpty (clusterPublicHost) && hazelcastClusterPort > 0) {
			publicAddress += clusterPublicHost + ":" + hazelcastClusterPort;
		}
		return publicAddress;
	}

	public static boolean getUndeployOnShutdown ()
	{
		return UndeployVerticlesOnHazlecastShutdown;
	}

}
