package com.ptc.ccp.vertxservices;

import java.util.List;

import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.ApplicationProperties.CommandLineArgument;
import com.sm.javax.utilx.CommandLineArguments;

import io.vertx.core.AsyncResult;
import io.vertx.servicediscovery.Record;

public class DiscoveryServiceMonitor extends AbstractPTCVerticle
{
	private enum Command {
		List, Remove
	}
	
	private Command command;
	private String  serviceName;
	
	public DiscoveryServiceMonitor (Command cmd, String serviceName)
	{
		super ();
		this.command = cmd;
		this.serviceName = serviceName;
	}

	@Override
	public void start () throws Exception
	{
		super.start ();
		findServices (rec -> serviceFilter (rec), res -> processCommand (res));
	}

	private boolean serviceFilter (Record rec)
	{
		if (! Strings.isEmpty (serviceName)) {
			return serviceName.equals (rec.getName ());
		}
		return true;
	}

	private void processCommand (AsyncResult<List<Record>> res)
	{
		if (res.succeeded ()) {
			List<Record> serviceRecords = res.result ();
			if (serviceRecords != null && serviceRecords.size () > 0) {
				for (Record record : serviceRecords) {
					System.out.println ("Service found: " + record.toJson ().encodePrettily ());
					if (command == Command.Remove) {
						System.out.println ("Removing ...");
						unpublishService (record);
					}
				}
			}
			else {
				System.out.println ("No registered servces found for you criteria.");
			}
		}
		else {
			System.out.println ("Failed to find any services: " + res.cause ());
		}
		terminate ();
	}

	// ==========================================================
	public static void main (String[] args)
	{
		ApplicationProperties.loadProperties (null, args, DiscoveryServiceMonitor.class);
		
		String serviceName = null;
		Command cmd = null;
		CommandLineArguments argList = ApplicationProperties.getCurrentCommandLineArgument (); 
		if (argList.hasArgument ("remove")) {
			cmd = Command.Remove;
			serviceName = argList.getArgument ("remove");
		}
		else if (argList.hasArgument ("list")){
			cmd = Command.List;
			serviceName = argList.getArgument ("list");
		}
		else {
			System.out.println ("Usage:");
			System.out.println (" -list[=<service name>] - list all service with the given name");
			System.out.println (" -remove=<service name> - unregister service with the given name");
			List<CommandLineArgument> cmdArgs = ApplicationProperties.getAllCommandLineArguments ();
			for (CommandLineArgument arg : cmdArgs) {
				System.out.println (" " + arg);
			}
		}
		
		if (cmd != null) {
			runClustered (new DiscoveryServiceMonitor (cmd, serviceName), buildVertxOptions ());
		}
	}

}
