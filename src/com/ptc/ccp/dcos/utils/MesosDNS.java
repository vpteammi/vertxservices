package com.ptc.ccp.dcos.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sm.javax.langx.Strings;

public class MesosDNS
{	
	private final String MESOS_DNS_URL = "http://master.mesos:8123/v1/";
	
	@SuppressWarnings ("unchecked")
	public List<SRVRecord> getServiceRecords (String serviceName)
	{
		System.out.println ("getServiceRecord() : " + serviceName);
		String serviceUrl = MESOS_DNS_URL + "/services/" + "_" + serviceName + "._tcp.marathon.mesos";
		System.out.println ("Service URL : " + serviceUrl);

		List<SRVRecord> records = new ArrayList<> ();
		try {

			String recordsJsonString = getForUrl (serviceUrl);

			if (!Strings.isEmpty (recordsJsonString)) {
				ObjectMapper mapper = new ObjectMapper ();
				Object value = mapper.readValue (recordsJsonString, new TypeReference<List<SRVRecord>> (){});
				if (value instanceof List<?>) {
					records = (List<SRVRecord>) value;
				}
				else {
					throw new Exception ();
				}
				// Check if it is fake record
				if (records.size () == 1) {
					SRVRecord record = records.get (0);
					if (record.getIp ().equals ("")) {
						records.clear ();
					}
				}
			}
		}
		catch (Exception ex) {
			System.err.println ("Failed to find DCOS Service Records  for service : " + serviceName);
		}

		return records;
	}

	private String getForUrl (String serviceUrl) throws Exception
	{
		URL metadataURL = new URL (serviceUrl);
		URLConnection connection = metadataURL.openConnection ();
		BufferedReader reader = new BufferedReader (new InputStreamReader (connection.getInputStream ()));

		String recordsJsonString = "";
		String inputLine;
		while ((inputLine = reader.readLine ()) != null) {
			recordsJsonString += inputLine;
		}
		reader.close ();
		return recordsJsonString;
	}
}
