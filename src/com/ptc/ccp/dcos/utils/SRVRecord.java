package com.ptc.ccp.dcos.utils;

public class SRVRecord {
	private String service;
	private String host;
	private String ip;
	private int port;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "SRVRecord [service=" + service + ", host=" + host + ", ip=" + ip + ", port=" + port + "]";
	}
}
