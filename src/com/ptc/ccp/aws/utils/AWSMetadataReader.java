package com.ptc.ccp.aws.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class AWSMetadataReader
{
	private final static String AWS_META_DATA_URL = "http://169.254.169.254/latest/meta-data/";
	
	public static String getAWSInstancePrivateIP (boolean loggingOn)
	{
		String privateIp = "";
		try {
			URL metadataURL = new URL (AWS_META_DATA_URL + "local-ipv4");
			URLConnection connection = metadataURL.openConnection ();
			BufferedReader reader = new BufferedReader (new InputStreamReader (connection.getInputStream ()));

			String inputLine;
			while ((inputLine = reader.readLine ()) != null) {
				if (loggingOn) {
					System.out.println (inputLine);
				}
				privateIp += inputLine;
			}

			reader.close ();
		}
		catch (Exception ex) {
			// ignore
			System.err.println ("Failed to read Private IP of AWS instace : " + ex.getMessage ());
		}

		if (loggingOn) {
			System.out.println ("AWS Instance Private IP : " + privateIp);
		}
		return privateIp;
	}
}

