package com.ptc.ccp.vertx.utils;

import com.sm.javax.langx.Strings;

import io.vertx.core.json.JsonObject;

public class JsonTools
{
	public static JsonObject buildSuccess (String msg)
	{
		JsonObject json = new JsonObject ().put ("success", true);
		if (! Strings.isEmpty (msg)) {
			json.put ("message", msg);
		}
		return json;
	}
	
	public static JsonObject buildSuccess ()
	{
		return buildSuccess (null);
	}
	
	public static JsonObject buildFail (String msg)
	{
		return new JsonObject ().put ("success", false).put ("message", msg);
	}

	public static JsonObject buildFail (Throwable err)
	{
		return new JsonObject ().put ("success", false).put ("message", err.getMessage ());
	}

}
