package com.ptc.ccp.vertx.utils;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.WorkerExecutor;
import io.vertx.core.impl.WorkerExecutorInternal;
import io.vertx.core.json.JsonObject;

/**
 * @author <a href="mailto:julien@julienviet.com">Julien Viet</a>
 */
public class VertxContextScheduler extends Scheduler {

  private final Vertx vertx;
  private final Context context;
  private final WorkerExecutor workerExecutor;
  private final boolean blocking;
  private final boolean ordered;

  public VertxContextScheduler(Context context, boolean blocking) {
    this(context, blocking, true);
  }

  public VertxContextScheduler(Context context, boolean blocking, boolean ordered) {
    Objects.requireNonNull(context, "context is null");
    this.vertx = context.owner();
    this.context = context;
    this.workerExecutor = null;
    this.blocking = blocking;
    this.ordered = ordered;
  }

  public VertxContextScheduler(Vertx vertx, boolean blocking) {
    this(vertx, blocking, true);
  }

  public VertxContextScheduler(Vertx vertx, boolean blocking, boolean ordered) {
    Objects.requireNonNull(vertx, "vertx is null");
    this.vertx = vertx;
    this.context = null;
    this.workerExecutor = null;
    this.blocking = blocking;
    this.ordered = ordered;
  }

  public VertxContextScheduler(WorkerExecutor workerExecutor) {
    this(workerExecutor, true);
  }

  public VertxContextScheduler(WorkerExecutor workerExecutor, boolean ordered) {
    Objects.requireNonNull(workerExecutor, "workerExecutor is null");
    this.vertx = ((WorkerExecutorInternal) workerExecutor).vertx();
    this.context = null;
    this.workerExecutor = workerExecutor;
    this.blocking = true;
    this.ordered = ordered;
  }

  public static Scheduler scheduler (Vertx vertx)
  {
	  return new VertxContextScheduler(vertx, false);
  }
  
  @Override
  public Worker createWorker() {
    return new WorkerImpl();
  }

  private static final Object DUMB = new JsonObject();

  private class WorkerImpl extends Worker {

    private final ConcurrentHashMap<TimedAction, Object> actions = new ConcurrentHashMap<>();
    private final AtomicBoolean cancelled = new AtomicBoolean();

    @Override
    public Disposable schedule(Runnable action) {
      return schedule(action, 0, TimeUnit.MILLISECONDS);
    }

    @Override
    public Disposable schedule(Runnable action, long delayTime, TimeUnit unit) {
      action = RxJavaPlugins.onSchedule(action);
      TimedAction timed = new TimedAction(action, unit.toMillis(delayTime), 0);
      actions.put(timed, DUMB);
      return timed;
    }

    @Override
    public Disposable schedulePeriodically(Runnable action, long initialDelay, long period, TimeUnit unit) {
      action = RxJavaPlugins.onSchedule(action);
      TimedAction timed = new TimedAction(action, unit.toMillis(initialDelay), unit.toMillis(period));
      actions.put(timed, DUMB);
      return timed;
    }

    @Override
    public void dispose() {
      if (cancelled.compareAndSet(false, true)) {
        actions.keySet().forEach(TimedAction::dispose);
      }
    }

    @Override
    public boolean isDisposed() {
      return cancelled.get();
    }

    class TimedAction implements Disposable, Runnable {

      private long id;
      private final Runnable action;
      private final long periodMillis;
      private boolean cancelled;

      public TimedAction(Runnable action, long delayMillis, long periodMillis) {
        this.cancelled = false;
        this.action = action;
        this.periodMillis = periodMillis;
        if (delayMillis > 0) {
          schedule(delayMillis);
        } else {
          id = -1;
          execute();
        }
      }

      private void schedule(long delay) {
        this.id = vertx.setTimer(delay, l -> execute());
      }

      private void execute() {
        if (workerExecutor != null) {
          workerExecutor.executeBlocking(fut -> {
            run();
            fut.complete();
          }, ordered, null);
          return;
        }
        Context ctx = context != null ? context : vertx.getOrCreateContext();
        if (blocking) {
          ctx.executeBlocking(fut -> {
            run();
            fut.complete();
          }, ordered, null);
        } else {
          ctx.runOnContext(v -> {
            run();
          });
        }
      }

      @Override
      public void run() {
        synchronized (TimedAction.this) {
          if (cancelled) {
            return;
          }
        }
        action.run();
        synchronized (TimedAction.this) {
          if (periodMillis > 0) {
            schedule(periodMillis);
          }
        }
      }

	@Override
	public void dispose ()
	{
        if (!cancelled) {
            actions.remove(this);
            if (id > 0) {
              vertx.cancelTimer(id);
            }
            cancelled = true;
          }
	}

	@Override
	public boolean isDisposed ()
	{
        return cancelled;
	}
    }
  }
}